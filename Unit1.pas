unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids,
  Vcl.ValEdit;

type
  TForm1 = class(TForm)
    Timer1: TTimer;
    ValueListEditor1: TValueListEditor;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FTexto: String;
    FThread1,FThread2: TThread;
    procedure ThreadProc1;
    procedure ThreadProc2;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  FThread1 := TThread.CreateAnonymousThread(ThreadProc1);
  FThread2 := TThread.CreateAnonymousThread(ThreadProc2);
  FThread1.Start;
  FThread2.Start;
end;

procedure TForm1.ThreadProc1;
  procedure faz(const aParam: String);
  begin
    //TThread.Queue(nil, procedure
    begin
      ValueListEditor1.Values[aParam] := DateTimeToStr(NOW);
    end
    //);
  end;
var
  vParam: String;
begin
  while (not Application.Terminated) do
  begin
    vParam := FTexto;
    if IsWindowVisible(Handle) then
      faz(vParam);
    Sleep(10)
  end;
end;
procedure TForm1.ThreadProc2;
begin
  while (not Application.Terminated) do
  begin
    TThread.Queue(FThread2, procedure begin
      FTexto := '';
    end);
    TThread.Queue(FThread2, procedure begin
      FTexto := FTexto + 'a';
    end);
    TThread.Queue(FThread2, procedure begin
      FTexto := FTexto + 'b';
    end);
    TThread.Queue(FThread2, procedure begin
      FTexto := FTexto + 'c';
    end);
    Sleep(100);
  end;
end;

end.
